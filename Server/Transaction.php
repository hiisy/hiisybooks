<?php

	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");

	//定義變數
	$userId = $_POST['userId'];	//使用者編號（primary key）
	$bookId = $_POST['bookId'];	//書本編號（primary key）
	$price = $_POST['price'];	//書本價格
	$limit = $_POST['limit'];	//書本持有期限（-1為永久）

	/*===== 插入資料庫 =====*/

	//欄位空格
	if(empty($userId) || empty($bookId) || !isset($price) || !isset($limit) )
	{
		echo "0";
		exit;
	}
	
	//不存在使用者或ID不可交易
	$options = array(
		'q' => array(
	        'userId' => $userId
	));
	$search = $db->search('User',$options);
	if(empty($search))
	{	
		echo "4"; //使用者ID不存在
		exit;
	}

	$options = array(
		'q' => array(
	        'bookId' => $bookId
	));
	$search = $db->search('Book',$options);
	if(empty($search))
	{
		echo "5"; //書本ID不存在
		exit;
	}

	//確認先前無此交易

	$options = array(
		'q' => array(
			'userId' => $userId,
	        'bookId' => $bookId
	));
	$search = $db->search('BookShelf',$options);
	if(!empty($search))
	{
		echo "3"; //持有此書
		exit;
	}

	//新增交易
	$data = array(
		'userId' => $userId,
		'bookId' => $bookId,
		'price' => $price,
		'limit' => $limit
	);
	$insert = $db->insert('BookShelf',$data);
	
	if(!empty($insert))
	{
		echo "1";//成功
	}
	else
	{
		echo "3"; //失敗
	}	


?>