<?php
	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");
	
	//定義變數
	$userId = $_POST['userId'];
	$userPwd = $_POST['userPwd'];

	/*===== 插入資料庫 =====*/

	//欄位空格
	if(empty($userId) || empty($userPwd))
	{
		echo "0"; //欄位為空
		exit;
	}
	
	//是否已經存在帳戶
	$options = array(
		'q' => array(
	        'userId' => $userId
	));
	$search = $db->search('User',$options);
	if(!empty($search))
	{
		echo "2"; // 帳號存在
		exit;
	}

	//插入帳號
	$data = array(
		'userId' => $userId,
		'userPwd' => $userPwd
	);
	$insert = $db->insert('User',$data);
	
	if($insert)
	{
		echo "1";//成功
	}
	else
	{
		echo "3"; //特殊錯誤
	}
?>