<?php

	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");

	//定義變數
	$bookId = $_POST['bookId'];			//書本編號（primary key）
	$title = $_POST['title'];	//書本標題
	$url = $_POST['url'];		//書本內容連結
	$date = $_POST['date'];		//書本上架日期

	/*===== 插入資料庫 =====*/

	//欄位空格
	if(empty($bookId) || empty($title) || empty($url) || empty($date) )
	{
		echo "0";
		exit;
	}
	
	//是否已經存在書本bookId
	$options = array(
		'q' => array(
	        'bookId' => $bookId
	));
	$search = $db->search('Book',$options);
	if(!empty($search))
	{
		echo "2"; // 書本bookId存在
		exit;
	}

	//新增書籍
	$data = array(
		'bookId' => $bookId,
		'title' => $title,
		'url' => $url,
		'date' => $date
	);
	$insert = $db->insert('Book',$data);
	
	if(!empty($insert))
	{
		echo "1";//成功
	}
	else
	{
		echo "3"; //特殊錯誤
	}	


?>