<?php

	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");
	
	//定義變數
	$userId = $_POST['userId'];	//使用者編號

	/*=====呼叫資料庫 =====*/
	
	//避免使用者不存在
	$options = array(
		'q' => array(
	        'userId' => $userId
	));
	$search = $db->search('User',$options);
	if(empty($search))
	{	
		echo "0"; //使用者ID不存在
		exit;
	}

	//呼叫使用者書架
	$options = array(
		'q' => array(
	        'userId' => $userId,
	));
	$search = $db->search('BookShelf',$options);
	echo json_encode($search);

?>