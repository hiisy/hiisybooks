<?php

	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");

	//定義變數
	$bookId = $_POST['bookId'];			//書本編號（primary key）

	/*===== 刪除資料庫 =====*/

	//欄位空格
	if(empty($bookId))
	{
		echo "0";
		exit;
	}
	
	//是否已經存在書本ID
	$options = array(
		'q' => array(
	        'bookId' => $bookId
	));
	$search = $db->search('Book',$options);
	if(empty($search))
	{
		echo "2"; // 書本ID不存在
		exit;
	}

	//刪除書籍
	$data = array(
		'bookId' => $bookId
	);
	$delete = $db->delete('Book',$data);
	if(empty($delete))
	{
		echo "3"; //特殊錯誤
		exit;
	}
	
	//用戶書籍刪除
	$options = array(
		'q' => array(
	        'bookId' => $bookId
	));
	$search = $db->search('BookShelf',$options);
	if(!empty($search))
	{
		$data = array(
		'bookId' => $bookId
		);
		$delete = $db->delete('BookShelf',$data);
		if(empty($delete))
		{
			echo "3"; //特殊錯誤
			exit;
		}
	}
	echo "1";//成功
?>