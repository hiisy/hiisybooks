<?php 
include 'mlabConfig.php';

class mlabRestClient {

    private $_apikey = '';
    private $_url = '';
    public $db = '';

    public function __construct($DB) {
        $mlabConfig = new mlabConfig;
        $this->_apikey = $mlabConfig::API_KEY;
        $this->_url = $mlabConfig::URL;
        $this->db = $DB;
    }

    public function search($table, $options = null) {
        /*
            /databases/{database}/collections/{collection}[q=<query>][&c=true][&f=<fields>][&fo=true][&s=<order>][&sk=<skip>][&l=<limit>]
        */
        $url = $this->_url.$this->db.'/collections/'.$table.'?';
        if ($options) {
            foreach ($options as $key => $value) {
                if ($value) {
                    $url = $url.$key.'=';
                    switch ($key) {
                        case 'q':
                        case 'f':
                        case 's':
                            $url = $url.json_encode($value);
                            break;

                        case 'c':
                        case 'fo':
                        case 'sk':
                        case 'l':
                            $url = $url.$value;
                            break;

                        default:
                            # do nothing......or raise error？
                            break;
                    }
                    $url = $url.'&';
                }
            }
        }

        $url = $url.'apiKey='.$this->_apikey;

        $params = array(
            'url' => $url,
            'method' => 'GET'
            );

        $response = $this->request($params);

        return $response;
    }

    public function insert($table, $data) {
        $url = $this->_url.$this->db.'/collections/'.$table.'?apiKey='.$this->_apikey;

        $data = json_encode($data);

        $params =  array(
            'url' => $url,
            'method' => 'POST',
            'type' => 'json',
            'data' => $data);

        $response = $this->request($params);

        return $response;
    }

    public function delete($table, $query = null) {
        $url = $this->_url.$this->db.'/collections/'.$table.'?';

        if ($query) {
            $url = $url.'q='.json_encode($query).'&';
        }

        $url = $url.'apiKey='.$this->_apikey;

        $params = array(
            'url' => $url,
            'method' => 'PUT',
            'type' => 'json',
            'data' => json_encode(array()));

        $response = $this->request($params);

        return $response;
    }

    private function request($args) {
        $url = $args['url'];
        if (isset($args['method']))
            $method = $args['method'];
        else
            $method = 'GET';
        if (isset($args['type']))
            $type = $args['type'];
        else
            $type = 'json';
        if (isset($args['data']))
            $content = $args['data'];
        else
            $content = NULL;

        $options = array(
            'http' => array(
                'header' => 'Content-Type: application/'.$type, 
                'method' => $method,
                'content' => $content));

        $context = stream_context_create($options);

        return json_decode(file_get_contents($url, true, $context));
    }
}
