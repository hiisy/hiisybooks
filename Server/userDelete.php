<?php

	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");

	//定義變數
	$userId = $_POST['userId'];//用戶ID

	/*===== 插入資料庫 =====*/

	//欄位空格
	if(empty($userId))
	{
		echo "0";
		exit;
	}
	
	//用戶ID是否存在
	$options = array(
		'q' => array(
	        'userId' => $userId
	));
	$search = $db->search('User',$options);
	if(empty($search))
	{
		echo "2"; // 用戶ID不存在
		exit;
	}

	//刪除用戶
	$data = array(
		'userId' => $userId
	);
	$delete = $db->delete('User',$data);
	if(empty($delete))
	{
		echo "3"; //特殊錯誤
		exit;
	}
	
	//用戶交易刪除
	$options = array(
		'q' => array(
	        'userId' => $userId
	));
	$search = $db->search('BookShelf',$options);
	
	if(!empty($search))
	{
		$data = array(
		'userId' => $userId
		);
		$delete = $db->delete('BookShelf',$data);

		if(empty($delete))
		{
			echo "3"; //特殊錯誤
			exit;	
		}
	}
	echo "1"; //成功
	exit;

?>