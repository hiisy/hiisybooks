<?php

	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");

	//定義變數
	$userId = $_POST['userId'];//用戶ID
	$bookId = $_POST['bookId'];//書籍ID

	/*===== 刪除資料庫 =====*/

	//欄位空格
	if(empty($userId) || empty($bookId))
	{
		echo "0";
		exit;
	}
	//確認為租借書籍且超過期限
	$options = array(
		'q' => array(
	        'bookId' => $bookId
	));
	$search = $db->search('BookShelf',$options);
	$search = (array)($search[0]);
	if( $search["limit"] == 0 )
	{
		//刪除交易
		$data = array(
			'userId' => $userId,
			'bookId' => $bookId
		);
		$delete = $db->delete('BookShelf',$data);
		if(!empty($delete))
		{
			echo "1"; //成功
		}
		else
		{
			echo "2"; //失敗
		}
	}
?>