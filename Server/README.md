﻿# mlabRestClient Document

使用 php 與 mlab RESTful API 溝通

## 環境設置

####設定 API KEY

```
class mlabConfig{
	const API_KEY = "xxxxxxxxxxx"; // Insert your API KEY
	const URL = "https://api.mlab.com/api/1/databases/";
}
```

#### 將 ```mlabConfig.php``` 和 ```mlab.php``` 一起放在server上

#### 在你的 php 檔案中加入以下指令
	
```
include('src to mlab.php')	# include mlab.php
```

#### 可以開始使用此 API 囉~

## 函式介紹
每個函式都會回傳資料庫端的 Response 喔～


### CREATE Object 

```
$db = new mlabRestClient('mydb');
```

### SELECT example

```
$db->search('User');	// 列出整個表格

$options = array(
	'q' => array(
		'user' => 'admin'),
	'c' => true,
	'f' => array(
		'user' => 1,
		'pwd' => 1),
	'fo' => true,
	's' => array(
		'user' => 1),
	'sk' => 20,
	'l' => 5);

$db->search('User', $options);	// 使用附加過濾器過濾
```

#### Option table

KEY 	| Value Type| Description
--------|-----------|------------
**q** 	| *Array* 	| restrict results by the specified JSON query
**c** 	| *Boolean* | the result count for this query 
**f** 	| *Array* 	| specify the set of fields to include or exclude in each document (1 - include; 0 - exclude)
**fo** 	| *Boolean* | return a single document from the result set (same as findOne() using the mongo shell
**s** 	| *Array* 	| specify the order in which to sort each specified field (1- ascending; -1 - descending)
**sk** 	| *Number*	| specify the number of results to skip in the result set; useful for paging
**l** 	| *Number* 	| specify the limit for the number of results (default is 1000)


### INSERT example

```
//	Insert 單筆資料
$data = array(
	'user' => 'admin',
	'pwd' => 'hello' );

// Insert 多筆資料

$data = array(
	array(
		'user' => 'admin',
		'pwd' => 'hello' ),
	array(
		'user' => 'guest',
		'pwd' => 'nonono' ),
	array(
		'user' => 'user',
		'pwd' => 'haha123' ),
	);

$db->insert('User', $data);
```

### DELETE example

```
$db->delete('User');	// 刪除整張表格

$query = array(
	'user' => 'guest',
	'pwd' => 'nonono');

$db->delete('User', $query) // 刪除過濾器過濾出的資料
```

#### Optional Query filter

**Type**		| *Array*
----------------|--------
**Description**	| restrict results by the specified JSON query




# PHP Server for Android

使用 mlabRestClient 編寫的 php 檔案 

#說明

##主要為三張 table，用戶（User）、書籍（Book）、交易（BookShelf）

###用戶資料：userId、userPwd

###書籍資料：bookId、title、url、date

###交易:userId、boolId、price、limit


功能|檔案
----|-----------------------------------------------------------------
用戶|新增用戶（register）、確認帳戶（login）、刪除用戶（userDelete）
書籍|新增書籍（bookInsert）、bookDelete（刪除書籍）
交易|新增交易（transaction）、租約到期/還書（tranactionEnd）
整理|書架（bookShelf）、排行（未完成）



##用戶功能

### register.php 註冊

####會顯示註冊成功或失敗訊息


情況	|代碼
--------|-----
欄位為空|0
註冊成功|1
帳號存在|2
特殊錯誤|3



### login.php 登入

####會顯示登入成功或失敗訊息


情況	  |代碼
----------|--------
欄位為空  |0
登入成功  |1
帳號不存在|2
密碼錯誤  |3


##userDelete.php 刪除用戶

####會顯示用戶刪除成功或失敗訊息，並且會連同 BookShelf 的相關用戶資料刪除


情況	|代碼
--------|---------
欄位為空|0
刪除成功|1
帳號存在|2
特殊錯誤|3
	


##書籍功能

### bookInsert.php 書籍新增

####會顯示書籍新增成功或失敗訊息


情況	|代碼
--------|-----
欄位為空|0
新增成功|1
書籍存在|2
特殊錯誤|3



### bookDelete.php 書籍刪除

####會顯示書籍刪除成功或失敗訊息


情況    |代碼
--------|--------
欄位為空|0
刪除成功|1
ID不存在|2
特殊錯誤|3


##交易功能

### transaction.php 交易

####會顯示交易成功或失敗訊息


情況	  |	代碼
----------|------
欄位為空  |	0
交易成功  |	1
交易失敗  |	2
已持有此書|	3
用戶不存在|	4
書籍不存在|	5



### transactionEnd.php 租約到期/還書

####會顯示租約是否成功取消訊息

情況	|代碼
--------|-----
欄位為空|0
解約成功|1
解約失敗|2


##整理功能

### bookShelf.php 註冊

####會顯示用戶書架，回傳json格式檔案

情況		|代碼
------------|-----
用戶不存在	|0
