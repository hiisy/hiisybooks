<?php
	//===== 連接資料 =====*/
	include 'mlab.php';
	$db = new mlabRestClient("hiisy");

	//定義變數
	$userId = $_POST['userId'];
	$userPwd = $_POST['userPwd'];

	//===== 認證帳戶 =====*/
	
	//欄位是否為空
	if(empty($userId) || empty($userPwd))
	{
		echo "0";
		exit;
	}

	//帳戶是否存在
	$options = array(
		'q' => array(
	        'userId' => $userId
	));
	$search = $db->search('User',$options);
	if(empty($search))
	{
		echo "2";
		exit;
	}

	//帳密檢驗
	$options = array(
		'q' => array(
	        'userId' => $userId,
	        'userPwd' => $userPwd
	));
	$search = $db->search('User',$options);
	if(empty($search))
	{
		echo "3";
	}
	else
	{
		echo "1";
	}
	exit;
?>