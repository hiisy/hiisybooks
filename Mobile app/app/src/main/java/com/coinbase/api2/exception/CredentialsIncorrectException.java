package com.coinbase.api2.exception;

public class CredentialsIncorrectException extends CoinbaseException {
    
    public CredentialsIncorrectException() {
        super("The provided credentials are invalid");
    }
    
}
