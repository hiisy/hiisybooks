package com.coinbase.api2.entity;

import com.coinbase.api2.entity.Account;

public class AccountResponse extends com.coinbase.api2.entity.Response {
    
    /**
     * 
     */
    private static final long serialVersionUID = -874896173060788591L;
    private com.coinbase.api2.entity.Account _account;
    
    public com.coinbase.api2.entity.Account getAccount() {
        return _account;
    }

    public void setAccount(Account account) {
        _account = account;
    }
}

