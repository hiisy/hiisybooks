package com.coinbase.api2.entity;

import java.io.Serializable;

public class AddressNode implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3331153178753180883L;
    private com.coinbase.api2.entity.Address _address;

    public com.coinbase.api2.entity.Address getAddress() {
        return _address;
    }

    public void setAddress(com.coinbase.api2.entity.Address address) {
        _address = address;
    }

}
