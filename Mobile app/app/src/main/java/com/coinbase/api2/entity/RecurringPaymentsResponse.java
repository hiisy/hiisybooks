package com.coinbase.api2.entity;

import java.util.List;

import com.coinbase.api2.deserializer.RecurringPaymentsLifter;
import com.coinbase.api2.entity.RecurringPayment;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class RecurringPaymentsResponse extends com.coinbase.api2.entity.Response {

    /**
     * 
     */
    private static final long serialVersionUID = 5547214480181926761L;
    private List<com.coinbase.api2.entity.RecurringPayment> _recurringPayments;

    public List<com.coinbase.api2.entity.RecurringPayment> getRecurringPayments() {
	return _recurringPayments;
    }

    @JsonDeserialize(converter=RecurringPaymentsLifter.class)
    public void setRecurringPayments(List<RecurringPayment> recurringPayments) {
	_recurringPayments = recurringPayments;
    }

}
