package com.coinbase.api2.exception;

public class CoinbaseException extends Exception {
    
    public CoinbaseException() {
	super();
    }
    
    public CoinbaseException(String message) {
	super(message);
    }
    
}
