package com.coinbase.api2.entity;

import java.util.List;

import com.coinbase.api2.deserializer.TransfersLifter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class TransfersResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = -812292521739103434L;
    private List<Transfer> _transfers;
    
    public List<Transfer> getTransfers() {
        return _transfers;
    }
    
    @JsonDeserialize(converter=TransfersLifter.class)
    public void setTransfers(List<Transfer> transfers) {
        _transfers = transfers;
    }
}
