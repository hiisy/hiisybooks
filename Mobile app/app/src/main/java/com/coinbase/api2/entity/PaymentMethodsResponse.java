package com.coinbase.api2.entity;

import java.util.List;

import com.coinbase.api2.deserializer.PaymentMethodsLifter;
import com.coinbase.api2.entity.PaymentMethod;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class PaymentMethodsResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = 4752885593284986181L;
    private String _defaultBuy;
    private String _defaultSell;
    private List<com.coinbase.api2.entity.PaymentMethod> _paymentMethods;

    public String getDefaultBuy() {
        return _defaultBuy;
    }

    public void setDefaultBuy(String defaultBuy) {
        _defaultBuy = defaultBuy;
    }

    public String getDefaultSell() {
        return _defaultSell;
    }

    public void setDefaultSell(String defaultSell) {
        _defaultSell = defaultSell;
    }

    public List<com.coinbase.api2.entity.PaymentMethod> getPaymentMethods() {
        return _paymentMethods;
    }

    @JsonDeserialize(converter=PaymentMethodsLifter.class)
    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        _paymentMethods = paymentMethods;
    }
}
