package com.coinbase.api2.deserializer;

import java.util.ArrayList;
import java.util.List;

import com.coinbase.api2.entity.RecurringPayment;
import com.fasterxml.jackson.databind.util.StdConverter;

public class RecurringPaymentsLifter extends StdConverter<List<com.coinbase.api2.entity.RecurringPaymentNode>, List<RecurringPayment>> {

    public List<RecurringPayment> convert(List<com.coinbase.api2.entity.RecurringPaymentNode> nodes) {
	ArrayList<RecurringPayment> result = new ArrayList<RecurringPayment>();
	
	for (com.coinbase.api2.entity.RecurringPaymentNode node : nodes) {
	    result.add(node.getRecurringPayment());
	}
	
	return result;
    }

}
