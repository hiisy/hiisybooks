package com.coinbase.api2.deserializer;

import java.util.ArrayList;
import java.util.List;

import com.coinbase.api2.entity.Address;
import com.fasterxml.jackson.databind.util.StdConverter;

public class AddressesLifter extends StdConverter<List<com.coinbase.api2.entity.AddressNode>, List<Address>> {

    public List<Address> convert(List<com.coinbase.api2.entity.AddressNode> nodes) {
	ArrayList<Address> result = new ArrayList<Address>();
	
	for (com.coinbase.api2.entity.AddressNode node : nodes) {
	    result.add(node.getAddress());
	}
	
	return result;
    }

}
