package com.coinbase.api2.entity;

public class TransactionResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = -8800221662555631113L;
    private Transaction _transaction;
    
    public Transaction getTransaction() {
        return _transaction;
    }

    public void setTransaction(Transaction transaction) {
        _transaction = transaction;
    }
}
