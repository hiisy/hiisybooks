package com.coinbase.api2.exception;

public class UnauthorizedDeviceException extends CoinbaseException {

    public UnauthorizedDeviceException() {
        super("Unconfirmed Device");
    }
    
}
