package hiisy.csie.ntnu.edu.tw.hiisybooks.backend;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface used to call Books services in the backend.
 */
public interface BooksVolumesService {
    @GET("volumes")
    Call<Volumes> getVolumes(
            @Query("q") String query,
            @Query("fields") String fields
    );
}
