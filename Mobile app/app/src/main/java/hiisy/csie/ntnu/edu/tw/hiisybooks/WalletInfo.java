package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.coinbase.android.sdk.OAuth;
import com.coinbase.api2.Coinbase;
import com.coinbase.api2.entity.OAuthTokensResponse;
import com.coinbase.api2.exception.UnauthorizedException;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WalletInfo extends BaseActivity {

    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.button)
    Button btnShowAddress;
    Handler h;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_info);
        // Setting for common menu
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        fab = (FloatingActionButton) findViewById(R.id.home);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), UserWeb.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        // Start from here

        ButterKnife.bind(this);
        h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(WalletInfo.this, "Hello World", Toast.LENGTH_SHORT).show();
            }
        }, 1000L);
        //coinbase sdk usage document link: https://github.com/coinbase/coinbase-java
        //coinbase with testnet account sign-up: https://sandbox.coinbase.com/signup
        //coinbase sandbox document: https://developers.coinbase.com/docs/wallet/testing
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("fintech", "got new intent");

        if (intent != null && intent.getAction() != null && intent.getAction().equals("android.intent.action.VIEW")) {
            Log.d("fintech", "ready to execute CompleteAuthorizationTask");
            new CompleteAuthorizationTask(intent).execute();
        }
    }

    @OnClick(R.id.button)
    public void login() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                //Coinbase cb = ((FintechApplication) getApplication()).getCoinbase();
                try {
                    //new CoinbaseBuilder().withBaseApiURL(new URL("https://api.sandbox.coinbase.com/")).withBaseOAuthURL(new URL("https://sandbox.coinbase.com/")).build();
                    OAuth.beginAuthorization(WalletInfo.this, Config.clientID, "user", "fintech://coinbase-oauth", null);
                } catch (Exception e) {
                    Log.e("fintech", null, e);
                    //Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
            }
        }.execute();
    }

    @OnClick(R.id.button2)
    public void showInfo() {
        new AsyncTask<Void, Void, Wallet>() {
            @Override
            protected Wallet doInBackground(Void... params) {
                Coinbase cb = ((FintechApplication) getApplication()).getCoinbase();
                Wallet w = new Wallet();
                try {
                    w.setAccount(cb.getUser().getEmail());
                    w.setAddress("address size = " + cb.getAddresses().getAddresses().size());
                    w.setBalance(cb.getBalance().toString());

                    Log.d("fintech", "address=" + w.getAddress());
                } catch (Exception e) {
                    Log.e("fintech", null, e);
                    //Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
                return w;
            }

            @Override
            protected void onPostExecute(Wallet w) {
                tvAccount.setText(w.getAccount());
                tvAddress.setText(w.getAddress());
                tvBalance.setText(w.getBalance());
            }
        }.execute();
    }

    public class CompleteAuthorizationTask extends AsyncTask<Void, Void, OAuthTokensResponse> {
        private Intent mIntent;

        public CompleteAuthorizationTask(Intent intent) {
            Log.d("fintech", "initial complete auth task");
            mIntent = intent;
        }

        @Override
        protected OAuthTokensResponse doInBackground(Void... params) {
            Log.d("fintech", "call complete authorization");
            try {
                return OAuth.completeAuthorization(WalletInfo.this, Config.clientID, Config.clientSecret, mIntent.getData());
            } catch (UnauthorizedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(OAuthTokensResponse r) {
            if (r == null) {
                Toast.makeText(WalletInfo.this, "Login fail", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(WalletInfo.this, "Login success", Toast.LENGTH_SHORT).show();
                ((FintechApplication) getApplication()).setOAuthResponse(r);
                Log.d("fintech", "token=" + r.getAccessToken());
            }
        }
    }


}
