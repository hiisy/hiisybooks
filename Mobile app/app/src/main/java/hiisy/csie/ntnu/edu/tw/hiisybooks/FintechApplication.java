package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.app.Application;
import android.util.Log;

import com.coinbase.android.sdk.OAuth;
import com.coinbase.api2.Coinbase;
import com.coinbase.api2.CoinbaseBuilder;
import com.coinbase.api2.entity.OAuthTokensResponse;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by USER on 2016/5/20.
 */
public class FintechApplication extends Application {
    protected OAuthTokensResponse authTokenResponse;
    protected Coinbase cb;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setOAuthResponse(OAuthTokensResponse authTokenResponse) {
        this.authTokenResponse = authTokenResponse;
    }

    public Coinbase getCoinbase() {
        if (cb == null) {
            try {
                Log.d("fintech","token="+authTokenResponse.getAccessToken());
                cb = new CoinbaseBuilder().withBaseApiURL(new URL(OAuth.URL_BASE_API_SANDBOX)).withBaseOAuthURL(new URL(OAuth.URL_BASE_OAUTH_SANDBOX))
                        .withAccessToken(authTokenResponse.getAccessToken())
                        .build();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return cb;
    }


}
