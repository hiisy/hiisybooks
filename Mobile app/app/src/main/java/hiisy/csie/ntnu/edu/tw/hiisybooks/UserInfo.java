package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserId;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserInfor;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserPwd;

public class UserInfo extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        // Setting for common menu
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        fab = (FloatingActionButton) findViewById(R.id.home);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), UserWeb.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        // Start from here
        userRecord = getSharedPreferences(UserInfor, MODE_PRIVATE);
        TextView textView = (TextView) findViewById(R.id.userinfor);
        textView.setText("@"+userRecord.getString(UserId, ""));
        EditText username = (EditText) findViewById(R.id.username_);
        username.setText(userRecord.getString(UserId, ""));
        EditText password = (EditText) findViewById(R.id.password_);
        password.setText(userRecord.getString(UserPwd, ""));
        Button button = (Button) findViewById(R.id.save);
        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder Msg = new AlertDialog.Builder(UserInfo.this);
                Msg.setMessage("確定更新？");
                Msg.setPositiveButton("確定", null);
                Msg.setNeutralButton("取消", null);
                Msg.show();
            }
        });
    }
}
