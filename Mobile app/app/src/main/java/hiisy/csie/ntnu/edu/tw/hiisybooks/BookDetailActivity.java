package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import hiisy.csie.ntnu.edu.tw.hiisybooks.backend.Item;

import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserId;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserInfor;

/**
 * An activity representing a single Book detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link BookListActivity}.
 */
public class BookDetailActivity extends AppCompatActivity {

    private Item mBook;
    private String price;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        mBook = getIntent().getParcelableExtra(BookDetailFragment.ARG_BOOK);
        price = getIntent().getStringExtra(BookDetailFragment.PRICE);
        SharedPreferences userRecord = getSharedPreferences(UserInfor, MODE_PRIVATE);
        userId = userRecord.getString(UserId, "");

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putParcelable(BookDetailFragment.ARG_BOOK, mBook);
            arguments.putString(BookDetailFragment.PRICE, price);
            arguments.putString(UserId, userId);
            BookDetailFragment fragment = new BookDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.book_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.book_detail, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, BookListActivity.class));
            return true;
        }

        if (id == R.id.book_buy) {
            BuyBooks();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void BuyBooks() {
        //System.out.println(userName.getText().toString());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://www.csie.ntnu.edu.tw/~40247008S/hiisy/transaction.php";
        System.out.println(mBook.volumeInfo.title);
        // Request to Server to authenticate user
        StringRequest stringRequest;
        stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        //LoginCheck(Integer.parseInt(response));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                //Toast.makeText(BookDetailFragment.this, "連結系統錯誤", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> hashMap = new HashMap<>();
                // hashMap.put("userId", userName.getText().toString());
                hashMap.put("userId", userId );
                hashMap.put("bookId", mBook.id );
                hashMap.put("price", price);
                hashMap.put("title",mBook.volumeInfo.title);

                return hashMap;
            }
        };
        // 設定Timeout error時間
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Toast.LENGTH_SHORT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
        //queue.start();
    }
}
