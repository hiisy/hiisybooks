package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.AutoLogin;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserId;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserInfor;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserPwd;

public class SignUp extends AppCompatActivity {
    EditText userName;
    EditText passWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ImageButton Register = (ImageButton) findViewById(R.id.Register);
        userName = (EditText) findViewById(R.id.newusername_);
        passWord = (EditText) findViewById(R.id.newpassword_);
        assert Register != null;
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register();
            }
        });

        //back2login
        ImageButton backLogin = (ImageButton) findViewById(R.id.back2login);
        assert backLogin != null;
        backLogin.setOnClickListener(new Button.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(View v) {
                                          // TODO Auto-generated method stub
                                          Intent intent = new Intent();
                                          intent.setClass(SignUp.this, HiisyLogin.class);
                                          startActivity(intent);
                                          SignUp.this.finish();
                                      }
                                  }
        );
    }

    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "Bye Bye!", Toast.LENGTH_SHORT).show();
    }

    private void Register() {
        System.out.println(userName.getText().toString());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://www.csie.ntnu.edu.tw/~40247008S/hiisy/register.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        if (Integer.parseInt(response) == 1) {
                            Toast.makeText(SignUp.this, "註冊成功", Toast.LENGTH_SHORT).show();
                            Login();
                        }
                        else {
                            RegisterError(Integer.parseInt(response));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> hashMap = new HashMap<>();
                hashMap.put("userId", userName.getText().toString());
                hashMap.put("userPwd", passWord.getText().toString());
                return hashMap;
            }
        };
        // 設定Timeout error時間
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Toast.LENGTH_SHORT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
    private void Login() {
        SharedPreferences userInfor = getSharedPreferences(UserInfor, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userInfor.edit();
        editor.putString(UserId, userName.getText().toString());
        editor.putString(UserPwd, passWord.getText().toString());
        editor.putBoolean(AutoLogin, false);
        editor.apply();
        Intent intent = new Intent();
        intent.setClass(SignUp.this, HiisyLogin.class);
        startActivity(intent);
        SignUp.this.finish();
    }

    private void RegisterError(int err) {
        AlertDialog.Builder ErrorMsg = new AlertDialog.Builder(SignUp.this);
        switch (err) {
            case 0:
                ErrorMsg.setMessage("資料未填寫完全");
                break;
            case 2:
                ErrorMsg.setMessage("此帳號已存在");
                break;
            case 3:
                ErrorMsg.setMessage("伺服器連結發生問題");
                break;
            default:
                ErrorMsg.setMessage("不知名的錯誤");
                break;
        }
        ErrorMsg.setNeutralButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                userName.setText("");
                passWord.setText("");
            }
        });
        ErrorMsg.show();
    }
}
