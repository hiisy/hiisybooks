package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import hiisy.csie.ntnu.edu.tw.hiisybooks.backend.BooksVolumesService;
import hiisy.csie.ntnu.edu.tw.hiisybooks.backend.Item;
import hiisy.csie.ntnu.edu.tw.hiisybooks.backend.Volumes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * An activity representing a list of Books. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link BookDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class BookListActivity extends AppCompatActivity {

    /**
     * For saving and restoring activity state.
     */
    private final static String ARG_BOOK_LIST = "book_list";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    /**
     * The adapter used to populate the list view.
     */
    private BookAdapter mAdapter;

    /**
     * The recycler view that shows the list of books.
     */
    private RecyclerView mRecyclerView;

    /**
     * The view to show in case the list of books is empty.
     */
    private TextView mEmptyView;

    /**
     * Retrofit object for HTTP requests to the backend.
     * Retrofit uses GSON for fast parsing of JSON data.
     */
    private Retrofit mRetrofit;

    /**
     * The Retrofit service to use when requesting Volumes webservice.
     */
    private BooksVolumesService mBooksVolumesService;

    //Button getBookId = (Button) findViewById(R.id.bought);
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //getBookId


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);
        mEmptyView = (TextView) findViewById(R.id.text_empty);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.home);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(BookListActivity.this, UserWeb.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mRecyclerView = (RecyclerView) findViewById(R.id.book_list);
        assert mRecyclerView != null;
        setupRecyclerView(mRecyclerView);

        if (findViewById(R.id.book_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        // Create the Retrofit objects that we use to make backend calls.
        mRetrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.url_google_books_base))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mBooksVolumesService = mRetrofit.create(BooksVolumesService.class);

        if (savedInstanceState != null) {
            ArrayList<Item> items = savedInstanceState.getParcelableArrayList(ARG_BOOK_LIST);
            mAdapter.setBooks(items);
        }

        setVisibility();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelableArrayList(ARG_BOOK_LIST, mAdapter.getBooks());
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Here we create the Search View in the app bar. When user enters text we perform
     * a search query on the backend for any books matching the user's input.
     *
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.search_view, menu);

        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        //searchView.onActionViewExpanded();
        //searchView.setIconified(false);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        mAdapter = new BookAdapter(this, mTwoPane);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            performQuery(query);
        }
    }

    /**
     * Perform REST operation against Google Books API using the current search query.
     *
     * @param query the plain text search query.
     */
    private void performQuery(String query) {
        // Here we invoke a query on the backend.
        // We pass the fields that we are interested in retrieving,
        //  which cuts down the amount of data transferred to the app.
        Call<Volumes> call = mBooksVolumesService.getVolumes(query, getString(R.string.volumes_fields));
        call.enqueue(new Callback<Volumes>() {
            @Override
            public void onResponse(Call<Volumes> call, Response<Volumes> response) {
                // Obtain the returned volumes from the response body.
                Volumes volumes = response.body();

                // Update the adapter with the new content.
                mAdapter.setBooks(volumes.items);
                mAdapter.notifyDataSetChanged();
                setVisibility();
            }

            @Override
            public void onFailure(Call<Volumes> call, Throwable t) {
                // Show error message, just for testing
                System.out.println(t.toString());

                mEmptyView.setText(t.getMessage());
                mAdapter.setBooks(new ArrayList<Item>());
                mAdapter.notifyDataSetChanged();
                setVisibility();
            }
        });
    }

    /**
     * Set visibility of various UI components depending on whether there is content.
     */
    private void setVisibility() {
        mRecyclerView.setVisibility(mAdapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
        mEmptyView.setVisibility(mAdapter.getItemCount() < 1 ? View.VISIBLE : View.GONE);
    }
}
