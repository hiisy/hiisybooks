package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;


public class UserWeb extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_web);
        // Setting for common menu
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        // Start from here
        Button sButton=(Button) findViewById(R.id.searchbook);
        assert  sButton !=null;
        sButton.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                RedirectTo(BookListActivity.class);
            }
        });

        final String mimetype = "text/html";
        final String encoding = "utf-8";
        WebView hot;
        hot = (WebView) findViewById(R.id.hot);
        assert hot != null;
        hot.loadData("<iframe style=\"height:500px;border:0px \" src=\"http://www.books.com.tw/\"></iframe> ", mimetype, encoding);

    }
}
