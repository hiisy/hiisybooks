package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.AutoLogin;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserId;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserInfor;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserPwd;


public class HiisyLogin extends AppCompatActivity {

    EditText userName;
    EditText password;
    CheckBox autoLogin;
    SharedPreferences userRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hiisy_login);
        userName = (EditText)findViewById(R.id.username_);
        password = (EditText)findViewById(R.id.password_);
        autoLogin = (CheckBox) findViewById(R.id.auto_login);
        userRecord = getSharedPreferences(UserInfor, Context.MODE_PRIVATE);
        ImageButton loginButton = (ImageButton) findViewById(R.id.LoginButton);
        Button button = (Button) findViewById(R.id.ApplyHiisy);

        if (userRecord.getBoolean(AutoLogin, false)) {
            RedirectTo(UserWeb.class);
        }
        else {
            userName.setText(userRecord.getString(UserId, ""));
            password.setText(userRecord.getString(UserPwd, ""));
        }

        //帳號登入
        assert loginButton != null;
        loginButton.setOnClickListener(new Button.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v) {
                                               Login();
                                           }
                                       }
        );

        // 註冊帳號
        assert button != null;
        button.setOnClickListener(new Button.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(View v) {
                                          RedirectTo(SignUp.class);
                                      }
                                  }
        );
    }

    private void Login() {
        System.out.println(userName.getText().toString());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://www.csie.ntnu.edu.tw/~40247008S/hiisy/login.php";
        // Request to Server to authenticate user
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        LoginCheck(Integer.parseInt(response));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                Toast.makeText(HiisyLogin.this, "連結系統錯誤", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> hashMap = new HashMap<>();
                hashMap.put("userId", userName.getText().toString());
                hashMap.put("userPwd", password.getText().toString());
                return hashMap;
            }
        };
        // 設定Timeout error時間
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Toast.LENGTH_SHORT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
        //queue.start();
    }

    private void LoginCheck(int response) {
        if(response == 1) {
            Toast.makeText(HiisyLogin.this, "登入成功", Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = userRecord.edit();
            editor.putString(UserId, userName.getText().toString());
            editor.putString(UserPwd, password.getText().toString());
            editor.putBoolean(AutoLogin, autoLogin.isChecked());
            editor.apply();
            RedirectTo(UserWeb.class);
        }
        else {
            LoginError(response);
        }
    }

    private void LoginError(int err) {
        AlertDialog.Builder ErrorMsg = new AlertDialog.Builder(HiisyLogin.this);
        switch (err) {
            case 0:
                ErrorMsg.setMessage("帳號或密碼未填寫");
                break;
            case 2:
                ErrorMsg.setMessage("帳號不存在");
                break;
            case 3:
                ErrorMsg.setMessage("密碼錯誤");
                break;
            default:
                ErrorMsg.setMessage("不知名的錯誤");
                break;
        }
        ErrorMsg.setNeutralButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                password.setText("");
            }
        });
        ErrorMsg.show();
    }
    private void RedirectTo(Class c) {
        // Redirect to another activities
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), c);
        startActivity(intent);
        HiisyLogin.this.finish();
    }

}
