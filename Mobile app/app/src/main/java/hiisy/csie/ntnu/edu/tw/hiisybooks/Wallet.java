package hiisy.csie.ntnu.edu.tw.hiisybooks;

/**
 * Created by USER on 2016/5/20.
 */
public class Wallet {
    private String account;
    private String address;
    private String balance;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
