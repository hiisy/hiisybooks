package hiisy.csie.ntnu.edu.tw.hiisybooks.backend;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * VolumeInfo in Books data.
 */
public class VolumeInfo implements Parcelable {
    public static final Parcelable.Creator<VolumeInfo> CREATOR = new Parcelable.Creator<VolumeInfo>() {
        @Override
        public VolumeInfo createFromParcel(Parcel source) {
            return new VolumeInfo(source);
        }

        @Override
        public VolumeInfo[] newArray(int size) {
            return new VolumeInfo[size];
        }
    };
    public String title;
    public String description;
    public ImageLinks imageLinks;

    public VolumeInfo() {
    }

    protected VolumeInfo(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.imageLinks = in.readParcelable(ImageLinks.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeParcelable(this.imageLinks, flags);
    }
}
