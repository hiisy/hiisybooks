package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hiisy.csie.ntnu.edu.tw.hiisybooks.backend.Item;

/**
 * Adapter to feed the BookListActivity with books from Google Books API.
 */
public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {

    /**
     * The Activity.
     */
    private AppCompatActivity mActivity;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    /**
     * List of books.
     */
    private ArrayList<Item> mBooks = new ArrayList<>();
    private String price;

    public BookAdapter(AppCompatActivity activity, boolean twoPane) {
        mActivity = activity;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mBooks.get(position);
        Picasso.with(mActivity).load(mBooks.get(position).volumeInfo.imageLinks.smallThumbnail).into(holder.mImageView);
        holder.mTitleView.setText(mBooks.get(position).volumeInfo.title);
        String s = mBooks.get(position).volumeInfo.title;
        Integer r = Math.abs(s.hashCode() % 1500 + 100);
        holder.mPrice = r.toString();
        //price = r.toString();
        System.out.println(r);
        holder.mMoney.setText("$ " + holder.mPrice);
        String id = mBooks.get(position).id;


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //得到書的title ::s


                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putParcelable(BookDetailFragment.ARG_BOOK, holder.mItem);
                    BookDetailFragment fragment = new BookDetailFragment();
                    fragment.setArguments(arguments);
                    mActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.book_detail_container, fragment)
                            .commit();
                } else {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, BookDetailActivity.class);
                    intent.putExtra(BookDetailFragment.ARG_BOOK, holder.mItem);
                    intent.putExtra(BookDetailFragment.PRICE, holder.mPrice);

                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBooks.size();
    }

    /**
     * Retrieve the books that the adapter manages.
     *
     * @return list of books.
     */
    public ArrayList<Item> getBooks() {
        return mBooks;
    }

    /**
     * Set the books that the adapter will manage.
     *
     * @param books list of books.
     */
    public void setBooks(ArrayList<Item> books) {
        mBooks = books;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTitleView;
        public final TextView mMoney;
        public Item mItem;
        public String mPrice;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.image);
            mTitleView = (TextView) view.findViewById(R.id.title);
            //
            mMoney = (TextView) view.findViewById(R.id.money);
        }
    }

    public Item getBookById(String id) {
        for(Item i:mBooks) {
            i.id.equals(id);
            return i;
        }
        return null;
    }
}
