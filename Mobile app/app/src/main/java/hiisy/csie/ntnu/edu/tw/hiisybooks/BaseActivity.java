package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserId;
import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserInfor;

public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    protected Toolbar toolbar;
    protected DrawerLayout drawer;
    protected ActionBarDrawerToggle toggle;
    protected NavigationView navigationView;
    protected FloatingActionButton fab;
    protected SharedPreferences userRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        //toolbar;
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        fab = (FloatingActionButton) findViewById(R.id.home);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), UserWeb.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        userRecord = getSharedPreferences(UserInfor, Context.MODE_PRIVATE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        // log out
        if (id == R.id.logout) {
            //userRecord = getSharedPreferences(UserInfor, Context.MODE_PRIVATE);
            System.out.println(userRecord.getString(UserId, "No User"));
            SharedPreferences.Editor editor = userRecord.edit();
            editor.clear();
            editor.apply();
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), HiisyLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent = new Intent();

        switch (id)
        {
            case R.id.book_buy: // 購買書籍
                intent.setClass(getApplicationContext(), BookBuy.class);
                startActivity(intent);
                setResult(Activity.RESULT_OK);
                break;
            case R.id.book_rent:    // 租用書籍
                break;
            case R.id.transaction_record:   // 交易紀錄
                intent.setClass(getApplicationContext(), TransactionRecord.class);
                startActivity(intent);
                setResult(Activity.RESULT_OK);
                break;
            case R.id.account_manage:   // 帳號管理
                intent.setClass(getApplicationContext(), UserInfo.class);
                startActivity(intent);
                setResult(Activity.RESULT_OK);
                break;
            case R.id.wallet_info:  // 錢包資訊
                intent.setClass(getApplicationContext(), WalletInfo.class);
                startActivity(intent);
                setResult(Activity.RESULT_OK);
                break;
            default:
                System.out.println(id);
                // Do nothing
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void RedirectTo(Class c) {
        // Redirect to another activities
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), c);
        startActivity(intent);
        //UserWeb.this.finish();
    }
}
