package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static hiisy.csie.ntnu.edu.tw.hiisybooks.UserData.UserId;

public class BookBuy extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_buy);
        // Setting for common menu
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        fab = (FloatingActionButton) findViewById(R.id.home);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), UserWeb.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        // Start from here
        /*final String username = userRecord.getString(UserId, "");
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://www.csie.ntnu.edu.tw/~40247008S/hiisy/bookShelf.php?userId="+username;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        System.out.println(response.toString());
                        try {
                           // GenerateBookList(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println(error.toString());
                    }
                });

        // 設定Timeout error時間
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonArrayRequest);
        queue.start();*/
    }

    /*private void GenerateBookList(JSONArray array) throws JSONException {
        ScrollView bookList = (ScrollView) findViewById(R.id.bookList);
        JSONObject o = array.getJSONObject(0);
        String author = o.getString("author");
        String title = o.getString("title");
        String description = o.getString("desc");
        //String img_url = o.getString("image");
        View v = LayoutInflater.from(BookBuy.this).inflate(R.layout.book_item, null);
        TextView bookname = (TextView) findViewById(R.id.bookname);
        TextView bookauthor = (TextView) findViewById(R.id.author);
        TextView bookdesc = (TextView) findViewById(R.id.description);
        assert bookname != null;
        bookname.setText("abc");
        assert bookname != null;
        bookname.setText(title);
        assert bookauthor != null;
        bookauthor.setText(author);
        assert bookdesc != null;
        bookdesc.setText(description);
        assert bookList != null;
        bookList.addView(v);
        for(int i=0; i<array.length(); i++) {
            JSONObject o = array.getJSONObject(i);
            String author = o.getString("author");
            String title = o.getString("title");
            String description = o.getString("desc");
            //String img_url = o.getString("image");
            View v = LayoutInflater.from(this).inflate(R.layout.book_item, null);
            TextView bookname = (TextView) findViewById(R.id.bookname);
            TextView bookauthor = (TextView) findViewById(R.id.author);
            TextView bookdesc = (TextView) findViewById(R.id.description);
            assert bookname != null;
            bookname.setText(title);
            assert bookauthor != null;
            bookauthor.setText(author);
            assert bookdesc != null;
            bookdesc.setText(description);
            assert bookList != null;
            bookList.addView(v);
        }
    }*/
}
