package hiisy.csie.ntnu.edu.tw.hiisybooks;

/**
 * Created by hane on 2016/6/16.
 */
public class UserData {
    public static final String UserInfor = "userInfor";
    public static final String UserId = "id";
    public static final String UserPwd = "pwd";
    public static final String AutoLogin = "autologin";
}
