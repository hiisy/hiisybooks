package hiisy.csie.ntnu.edu.tw.hiisybooks;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import hiisy.csie.ntnu.edu.tw.hiisybooks.backend.Item;

/**
 * A fragment representing a single Book detail screen.
 * This fragment is either contained in a {@link BookListActivity}
 * in two-pane mode (on tablets) or a {@link BookDetailActivity}
 * on handsets.
 */
public class BookDetailFragment extends Fragment {
    private String userId;
    private String price;
    public static final String PRICE = "price";
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_BOOK = "book";

    /**
     * Number of lines to indent when rendering text around the image.
     * To improve we should calculate this based on the font size and image height.
     */
    private static final int SPANNABLE_LINES = 5;

    /**
     * The dummy content this fragment is presenting.
     */
    private Item mBook;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BookDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_BOOK)) {
            // Load the book specified by the fragment arguments.
            mBook = getArguments().getParcelable(ARG_BOOK);

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mBook.volumeInfo.title);
            }
        }
        price = getArguments().getString(PRICE);
        userId= getArguments().getString(UserData.UserId);
        //System.out.println(userId);
    }
    Integer r;
    String s;
    String pic;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.book_detail, container, false);
        if (mBook == null || mBook.volumeInfo == null) {
            return rootView;
        }

        TextView bookMoney = (TextView) getActivity().findViewById(R.id.moneyBook);
        //s =mBook.volumeInfo.title;
        //r = Math.abs(s.hashCode() % 1500 + 100);
        bookMoney.setText(bookMoney.getText() + price);

        pic=mBook.volumeInfo.imageLinks.thumbnail;
        // Set the thumbnail.
        Picasso.with(getActivity()).load(mBook.volumeInfo.imageLinks.thumbnail).into(((ImageView) rootView.findViewById(R.id.image)));

        // Set the description text
        if (mBook.volumeInfo.description == null) {
            return rootView;
        }

        // Wrap text around the image.
        // See http://stackoverflow.com/questions/11494158/how-to-align-textview-around-an-imageview
        float textWrapIndent = getActivity().getResources().getDimension(R.dimen.text_wrap_indent);
        SpannableString ss = new SpannableString(mBook.volumeInfo.description);
        ss.setSpan(new LeadingMarginSpan2(SPANNABLE_LINES, (int) textWrapIndent), 0, ss.length(), 0);
        ((TextView) rootView.findViewById(R.id.description)).setText(ss);

        return rootView;
    }
    //---------------add---------------

/*
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final MenuItem buyBook= (MenuItem) getActivity().findViewById(R.id.book_buy);
        assert buyBook != null;
        buyBook.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                BuyBooks();
                System.out.println(mBook.volumeInfo.title);
                return true;
            }
        });
    }

    protected void BuyBooks() {
        //System.out.println(userName.getText().toString());
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url ="http://www.csie.ntnu.edu.tw/~40247008S/hiisy/transaction.php";
        System.out.println(mBook.volumeInfo.title);
        // Request to Server to authenticate user
        StringRequest stringRequest;
        stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        //LoginCheck(Integer.parseInt(response));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                //Toast.makeText(BookDetailFragment.this, "連結系統錯誤", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> hashMap = new HashMap<>();
                // hashMap.put("userId", userName.getText().toString());
                hashMap.put("userId", userId );
                hashMap.put("bookId", mBook.id );
                hashMap.put("price", price);
                hashMap.put("title",mBook.volumeInfo.title);

                return hashMap;
            }
        };
        // 設定Timeout error時間
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Toast.LENGTH_SHORT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
        //queue.start();
    }*/
}
